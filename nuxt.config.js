module.exports = {
  /*
  ** set caches
  */
  cache: true,
  /*
  ** Headers of the page
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy'
  ],
  axios: {
    errorHandler (errorReason, { error }) {
      error('Request Error: ' + errorReason)
    }
  },
  proxy: [
    ['/api',{ target: process.env.PROXY_API_URL || 'http://rap.taobao.org/mockjsdata/30077/api', pathRewrite: { '^/api': '/beijing' } }]
  ],
  /*
  ** Headers of the page
  */
  head: {
    title: '尚层装饰_别墅装修设计公司 别墅装饰领军企业——尚层装饰官网',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { httpEquiv: 'x-ua-compatiable', content: 'IE=edge' },
      { hid: 'keywords', name: 'keywords', content: '别墅装修，别墅设计，高端别墅装修，别墅装饰，高端别墅装饰，高端别墅设计' },
      { hid: 'description', name: 'description', content: '尚层装饰，中国别墅装饰领军企业,高端别墅装修设计解决专家，提供别墅装饰整体规划设计与施工、别墅设备、主材、家具、配饰私属定制服务。尚层装饰热线:400-650-2118' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: '/css/reset.css' }
    ],
    script: [
      // { hid: 'mock', src: 'http://rap.taobao.org/rap.plugin.js?projectId=30077' }
    ]
  },
  /*
  ** global css
  */
  css: [
    // '~assets/reset.css'
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#D7000F', height: '3px' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    babel: {
      presets: ['es2015', 'stage-0'],
      plugins: ['transform-runtime']
    },
    vendor: [
      // 'rap-global-proxy'
    ]
  },
  plugins: [
    // {src: '~plugins/rap-global-proxy'}
  ]
}
