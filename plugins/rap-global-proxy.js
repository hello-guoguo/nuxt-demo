import Vue from 'vue'
import rapGlobalProxy from 'rap-global-proxy'
import axios from 'axios'
import VueAxios from 'vue-axios'

rapGlobalProxy.doProxy({ url: 'http://rap.taobao.org/mockjsdata/', projectId: 30077 })
Vue.use(rapGlobalProxy)
Vue.use(VueAxios, axios)
